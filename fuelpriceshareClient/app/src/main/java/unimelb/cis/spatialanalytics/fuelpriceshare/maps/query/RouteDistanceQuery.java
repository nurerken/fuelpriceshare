package unimelb.cis.spatialanalytics.fuelpriceshare.maps.query;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Nurlan Kenzhebekov on 24.04.2015.
 * The class is used to calculate distance between two locations on the map
 */
public class RouteDistanceQuery{

    /**
     * Calculates Euclidean distance between two locations using Haversine formula.
     * @param origin -- geolocation of source point
     * @param destination -- geolocation of destination point
     * @return -- Euclidean distance between two points
     */

    public static double getEuclideanDistance(LatLng origin, LatLng destination){
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(destination.latitude - origin.latitude);
        double dLng = Math.toRadians(destination.longitude - origin.longitude);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(origin.latitude)) * Math.cos(Math.toRadians(destination.latitude)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist =  (earthRadius * c);

        return dist;
    }
}

