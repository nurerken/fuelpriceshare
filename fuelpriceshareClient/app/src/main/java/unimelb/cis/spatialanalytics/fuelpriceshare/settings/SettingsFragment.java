package unimelb.cis.spatialanalytics.fuelpriceshare.settings;

import  android.support.v4.preference.PreferenceFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import unimelb.cis.spatialanalytics.fuelpriceshare.R;

/**
 * Created by Yu Sun on 19/02/2015.
 * PreferenceFragment is only supported for Android 3.0 (API level 11).
 * Currently, the API level we use is 10, which supports 99.3% of devices.
 * If we need this feature, we can increase the API level by one, which
 * can support 87% of devices.
 */

// Modified by Nurlan Kenzhebekov on 25.03.2015
// Replace PreferenceActivity with PreferenceFragment using support v4 library

public class SettingsFragment extends PreferenceFragment {

    public SettingsFragment(){
        //System.out.println();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.pref_general);
    }
}
