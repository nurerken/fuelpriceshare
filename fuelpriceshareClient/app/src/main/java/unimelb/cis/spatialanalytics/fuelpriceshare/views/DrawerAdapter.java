package unimelb.cis.spatialanalytics.fuelpriceshare.views;

import unimelb.cis.spatialanalytics.fuelpriceshare.R;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/*Created by Nurlan Kenzhebekov on 24.03.2015
* This class is used to create customized listview with icons
* */
public class DrawerAdapter extends ArrayAdapter<String>{
    private final Activity context;
    private final String[] asText;
    private final Integer[] anImageID;
    private int nLayoutID, nTextViewID, nImgViwID;

    public DrawerAdapter(Activity context, String[] asText, Integer[] anImageID, int nLayoutID, int nTextViewID, int nImgViwID) {
        super(context, nLayoutID, asText);
        this.context = context;
        this.asText = asText;
        this.anImageID = anImageID;
        this.nLayoutID = nLayoutID;
        this.nTextViewID = nTextViewID;
        this.nImgViwID = nImgViwID;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(nLayoutID, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(nTextViewID);
        ImageView imageView = (ImageView) rowView.findViewById(nImgViwID);
        txtTitle.setText(asText[position]);
        imageView.setImageResource(anImageID[position]);
        return rowView;
    }
}