package unimelb.cis.spatialanalytics.fuelpriceshare;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Stack;

import unimelb.cis.spatialanalytics.fuelpriceshare.config.URLConstant;
import unimelb.cis.spatialanalytics.fuelpriceshare.data.Users;
import unimelb.cis.spatialanalytics.fuelpriceshare.fragment.ContributePriceFragment;
import unimelb.cis.spatialanalytics.fuelpriceshare.fragment.HelpFragment;
import unimelb.cis.spatialanalytics.fuelpriceshare.fragment.MapFragment;
import unimelb.cis.spatialanalytics.fuelpriceshare.fragment.ProfileFragment;
import unimelb.cis.spatialanalytics.fuelpriceshare.maps.DrawOnMap.DrawMarkersOnMap;
import unimelb.cis.spatialanalytics.fuelpriceshare.maps.myLocation.MyLocation;
import unimelb.cis.spatialanalytics.fuelpriceshare.others.MyLog;
import unimelb.cis.spatialanalytics.fuelpriceshare.settings.SettingsFragment;
import unimelb.cis.spatialanalytics.fuelpriceshare.views.DrawerAdapter;
/**
 * Created by Yu Sun on 17/02/2015.
 * We release the prototype 1.0 on 04/03/2015.
 */
public class MainActivity extends ActionBarActivity implements ContributePriceFragment.setDefaultView{

    private final String LOG_TAG = MainActivity.class.getSimpleName();

    private ActionBar actionBar;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private String[] mDrawerItemStrings;

    private static final int FRAGMENT_MAP = 0;
//    private static final int FRAGMENT_RANGE_SEARCH = 1;
//    private static final int FRAGMENT_PATH_SEARCH = 2;

    private static final int FRAGMENT_PROFILE = 1;
    private static final int FRAGMENT_CONTRIBUTE = 2;
    private static final int FRAGMENT_SETTING = 3;
    private static final int FRAGMENT_HELP = 4;

    private Fragment fragment = null;
    private MapFragment mapFragment = null;
    private Fragment profileFragment = null;
    private Fragment contributeFragment = null;
    private Fragment settingsFragment = null;
    private Fragment helpFragment = null;// Added by Nurlan Kenzhebekov 24.04.2015

    private int PRESENT_FRAGMENT_ID;
    private final String PRESENT_FRAGMENT_ID_KEY = "present_fragment_id";
    private final String LAUNCH_TIME_KEY = "launch_time";
    private final int MAX_TIME_SHOW_DRAWER_AFTER_LAUNCH = 1;
    private SharedPreferences pref;

    // Added by Nurlan Kenzhebekov on 23.04.2015
    // Used to determine user's location
    private MyLocation myLocation = null;

    // added by Han Li on 07/04/2015
    public static ContributePriceFragment.setDefaultView setDefaultViewInterFace;

    // added by Nurlan on 05.05.2015. To make the app more intuitive we should
    // keep the fragment navigation history. When user press the back button
    // system should go to previous fragment opened. App quits when there the
    // stack is empty, i.e. when user returned back to initial home page (mapfragment)
    private Stack<Integer> fragmentHistory = null;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        // MyLog.e(LOG_TAG, "on create");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentHistory = new Stack<Integer>();
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerItemStrings = getResources().getStringArray(R.array.nav_drawer_items);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        // Nurlan Kenzhebekov. 24.03.2015 added icons to the listview
        Integer[] anImageID = {
                R.drawable.around,
                R.drawable.profile,
                R.drawable.price,
                R.drawable.preferences,
                R.drawable.ic_help
        };

        DrawerAdapter adapter = new DrawerAdapter(this, mDrawerItemStrings, anImageID, R.layout.drawer_list_item, android.R.id.text1, R.id.img);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        actionBar = this.getSupportActionBar();
//
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
// Yu Sun 19/02/2015 No need to use for v7.  R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                actionBar.setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                actionBar.setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // Yu Sun: always close the soft key-pad after launch
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        setUpFragments();
        myLocation = new MyLocation(this.getApplicationContext());

        if(!myLocation.initFusedLocationClient()){
            AlertDialog ad = new AlertDialog.Builder(this).create();
            ad.setCancelable(false); // This blocks the 'BACK' button
            ad.setMessage("Google Play Service is not available. Please install and enable it");
            ad.setButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            ad.show();

            finish();
        }
        myLocation.setUpCurrentLocation();

        pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (savedInstanceState == null) {
            // Yu Sun 02/03/2015: Changed according to Rui's comments
            //int last_display_fragment_id = 0;
            //last_display_fragment_id = pref.getInt(PRESENT_FRAGMENT_ID_KEY, 0);
            //selectItem(last_display_fragment_id);
            selectItem(0, false);

            int launchTime = pref.getInt(LAUNCH_TIME_KEY, 0);
            //Log.v(LOG_TAG, "Launch time:" + launchTime);
            if (launchTime < MAX_TIME_SHOW_DRAWER_AFTER_LAUNCH) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
                pref.edit().putInt(LAUNCH_TIME_KEY, ++launchTime).commit();
                pref.edit().apply();
            }
        }

        // Added by Nurlan on 13.05.2015. Need to send log file to server
        MyLog.d(LOG_TAG, "MainActivity: OnCreate invoked. UserID: " + Users.id);
        new sendFile().execute(new String[] {""});
    }

    private void setUpFragments() {

        /////////// Create all the fragments to be used ////////////
        mapFragment = new MapFragment();
        profileFragment = new ProfileFragment();
        contributeFragment = new ContributePriceFragment();
        settingsFragment = new SettingsFragment();
        helpFragment = new HelpFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu items for use in the action bar
        // MenuInflater inflater = getMenuInflater();
        // inflater.inflate(R.menu.menu_navdrawer, menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

//        // If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
//        return super.onPrepareOptionsMenu(menu);

        // If the nav drawer is open, hide action items related to the content view

        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);

        /**
         * Handle Fragment Menu if special requirements are needed
         */
        switch (PRESENT_FRAGMENT_ID) {
            case FRAGMENT_MAP:
                break;
//            case FRAGMENT_RANGE_SEARCH:
//                break;
//            case FRAGMENT_PATH_SEARCH:
//                break;
            case FRAGMENT_CONTRIBUTE:
                if (drawerOpen)
                    menu.setGroupVisible(R.id.menu_group, false);
                else
                    menu.setGroupVisible(R.id.menu_group, ContributePriceFragment.isMenuVisible);
                break;
            case FRAGMENT_PROFILE:
                break;
            default:
                MyLog.e(LOG_TAG, "No such case id");
                break;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    // Handle presses on the action bar items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position, false);
        }
    }

    private void selectItem(int position, boolean bBackButtonInvoked) {

        // update the main content by replacing fragments

        // Han Li and Yu Sun 26/02/2015: close the soft keypad
        if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null) {
            InputMethodManager keyboard = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        if(!bBackButtonInvoked && fragment != null &&(fragmentHistory.isEmpty() || fragmentHistory.lastElement() != PRESENT_FRAGMENT_ID))
            fragmentHistory.push(PRESENT_FRAGMENT_ID);

        PRESENT_FRAGMENT_ID = position;
        //if(position!=FRAGMENT_CONTRIBUTE)
        // ContributePriceFragment.isMenuVisible=false;

//        if (mapFragment != null && rangeFragment != null && pathFragment != null
//                && contributeFragment != null && profileFragment != null ) {
        if (mapFragment != null && contributeFragment != null && profileFragment != null
                && settingsFragment != null && helpFragment != null) {

            //FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            //fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(mDrawerItemStrings[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

            fragment = null;
            switch (position) {
                case FRAGMENT_MAP:
                    if (mapFragment.isAdded()) {
                        fragmentTransaction.show(mapFragment);
                    } else {
                        //fragmentTransaction.add(R.id.content_frame, mapFragment);
                        //changed by Han : to add flag to the fragment that can be traced later
                        //back to the MapFragment from ContributePriceFragment
                        fragmentTransaction.add(R.id.content_frame, mapFragment, String.valueOf(FRAGMENT_MAP));
                    }
                    if (profileFragment.isAdded()) {
                        fragmentTransaction.hide(profileFragment);
                    }
                    if (contributeFragment.isAdded()) {
                        fragmentTransaction.hide(contributeFragment);
                    }
                    if (settingsFragment.isAdded()) {
                        fragmentTransaction.hide(settingsFragment);
                    }
                    if (helpFragment.isAdded()) {
                        fragmentTransaction.hide(helpFragment);
                    }
                    fragment = mapFragment;
                    break;
//                case FRAGMENT_RANGE_SEARCH:
//                    if( rangeFragment.isAdded() ){
//                        fragmentTransaction.show(rangeFragment);
//                    }else{
//                        fragmentTransaction.add(R.id.content_frame, rangeFragment);
//                    }
//                    if(pathFragment.isAdded()){fragmentTransaction.hide(pathFragment);}
//                    if(profileFragment.isAdded()){fragmentTransaction.hide(profileFragment);}
//                    if(contributeFragment.isAdded()){fragmentTransaction.hide(contributeFragment);}
//                    fragment = rangeFragment;
//                    break;
//                case FRAGMENT_PATH_SEARCH:
//                    if( pathFragment.isAdded() ){
//                        fragmentTransaction.show(pathFragment);
//                    }else{
//                        fragmentTransaction.add(R.id.content_frame, pathFragment);
//                    }
//                    if(rangeFragment.isAdded()){fragmentTransaction.hide(rangeFragment);}
//                    if(profileFragment.isAdded()){fragmentTransaction.hide(profileFragment);}
//                    if(contributeFragment.isAdded()){fragmentTransaction.hide(contributeFragment);}
//                    fragment = pathFragment;
//                    break;
                case FRAGMENT_PROFILE:
                    if (profileFragment != null)
                        ((ProfileFragment) profileFragment).updateCredit();
                    if (profileFragment.isAdded()) {
                        fragmentTransaction.show(profileFragment);
                    } else {
                        fragmentTransaction.add(R.id.content_frame, profileFragment);
                    }
//                    if(rangeFragment.isAdded()){fragmentTransaction.hide(rangeFragment);}
//                    if(pathFragment.isAdded()){fragmentTransaction.hide(pathFragment);}
                    if (mapFragment.isAdded()) {
                        fragmentTransaction.hide(mapFragment);
                    }
                    if (contributeFragment.isAdded()) {
                        fragmentTransaction.hide(contributeFragment);
                    }
                    if (settingsFragment.isAdded()) {
                        fragmentTransaction.hide(settingsFragment);
                    }
                    if (helpFragment.isAdded()) {
                        fragmentTransaction.hide(helpFragment);
                    }
                    fragment = profileFragment;
                    break;
                case FRAGMENT_CONTRIBUTE:
                    if (contributeFragment.isAdded()) {
                        fragmentTransaction.show(contributeFragment);
                    } else {
                        fragmentTransaction.add(R.id.content_frame, contributeFragment);
                    }
//                    if(rangeFragment.isAdded()){fragmentTransaction.hide(rangeFragment);}
//                    if(pathFragment.isAdded()){fragmentTransaction.hide(pathFragment);}
                    if (mapFragment.isAdded()) {
                        fragmentTransaction.hide(mapFragment);
                    }
                    if (profileFragment.isAdded()) {
                        fragmentTransaction.hide(profileFragment);
                    }
                    if (settingsFragment.isAdded()) {
                        fragmentTransaction.hide(settingsFragment);
                    }
                    if (helpFragment.isAdded()) {
                        fragmentTransaction.hide(helpFragment);
                    }
                    fragment = contributeFragment;
                    break;
                case FRAGMENT_SETTING:
                    if(settingsFragment.isAdded()){
                        fragmentTransaction.show(settingsFragment);
                    }else{
                        fragmentTransaction.add(R.id.content_frame, settingsFragment);
                    }
                    if (mapFragment.isAdded()) {
                        fragmentTransaction.hide(mapFragment);
                    }
                    if (profileFragment.isAdded()) {
                        fragmentTransaction.hide(profileFragment);
                    }
                    if (contributeFragment.isAdded()) {
                        fragmentTransaction.hide(contributeFragment);
                    }
                    if (helpFragment.isAdded()) {
                        fragmentTransaction.hide(helpFragment);
                    }
                    fragment = settingsFragment;
                    break;
                case FRAGMENT_HELP:
                    if(helpFragment.isAdded()){
                        fragmentTransaction.show(helpFragment);
                    }else{
                        fragmentTransaction.add(R.id.content_frame, helpFragment);
                    }
                    if (mapFragment.isAdded()) {
                        fragmentTransaction.hide(mapFragment);
                    }
                    if (profileFragment.isAdded()) {
                        fragmentTransaction.hide(profileFragment);
                    }
                    if (contributeFragment.isAdded()) {
                        fragmentTransaction.hide(contributeFragment);
                    }
                    if (settingsFragment.isAdded()) {
                        fragmentTransaction.hide(settingsFragment);
                    }
                    fragment = helpFragment;
                    break;
                default:
                    break;
            }

            fragmentTransaction.commit();
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            setTitle(mDrawerItemStrings[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        actionBar.setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();

        // Added by Han Li on 07/04/2015
        setDefaultViewInterFace=this;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Receive all the returning results of children activities. Detailed information please refer to the
     * official android programming document
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        /**
         * Directly call the present fragment to handle onActivityResult
         */
//        if( contributeFragment.isAdded() )
//            contributeFragment.onActivityResult(requestCode, resultCode, intent);
//        if( profileFragment.isAdded() )
//            profileFragment.onActivityResult(requestCode, resultCode, intent);
        if (fragment != null)
            fragment.onActivityResult(requestCode, resultCode, intent);

    }

    @Override
    protected void onDestroy() {

        //MyLog.e(LOG_TAG, "on destroy");

        super.onDestroy();
        pref.edit().putInt(
                PRESENT_FRAGMENT_ID_KEY, PRESENT_FRAGMENT_ID
        ).commit();
        pref.edit().apply();

        // added by Yu Sun 06/04/2015
        DrawMarkersOnMap.clearStations();
    }

    @Override
    public void setDefaultView(int fragmentId) {
        selectItem(fragmentId, false);
    }

    // Updated by Nurlan Kenzhebekov on 24.04.2015
    // Stop and continue location updates according to activity life cycle.

    @Override
    protected void onResume() {
        super.onResume();
        try{
            if (myLocation.mGoogleApiClient.isConnected()) {
                myLocation.startLocationUpdates();
            }
        }catch(Exception ex){
            MyLog.d(LOG_TAG, ex.getMessage());}
    }

    @Override
    protected void onStart() {
        super.onStart();
        try{
            if(!myLocation.mGoogleApiClient.isConnected()){
                myLocation.mGoogleApiClient.connect();
            }
        }catch(Exception ex){MyLog.d(LOG_TAG, ex.getMessage());}
    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            myLocation.stopLocationUpdates();
        }catch(Exception ex){MyLog.d(LOG_TAG, ex.getMessage());}
    }

    @Override
    protected void onStop() {
        try{
            if(myLocation.mGoogleApiClient.isConnected())
                myLocation.mGoogleApiClient.disconnect();
        }catch(Exception ex){MyLog.d(LOG_TAG, ex.getMessage());}
        super.onStop();
    }

    // Added by Nurlan Kenzhebekov on 08.05.2015
    // Before quiting the app first return back to previous fragment recorded at fragmentHistory
    // if current fragment is mapfragment then check if waypointsliding or directionsliding is open, if so close them
    // else clear map before quiting
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        MyLog.d("d", "Button pressed");
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if(fragment != null && PRESENT_FRAGMENT_ID == FRAGMENT_MAP){
                if(mapFragment.directionSliding != null){
                    if(mapFragment.directionSliding.getPanelState() == SlidingUpPanelLayout.PanelState.ANCHORED ){
                        mapFragment.directionSliding.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                        return true;
                    }
                }
                if(mapFragment.wayPointSliding != null){
                    if(mapFragment.wayPointSliding.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN){
                        mapFragment.wayPointSliding.setPanelState(SlidingUpPanelLayout.PanelState.HIDDEN);
                        return true;
                    }
                }
             }

            if(!fragmentHistory.isEmpty()){
                selectItem(fragmentHistory.pop(), true);
                return true;
            }
            else{
                boolean bAreStationsNullOrEmpty = false;
                if(DrawMarkersOnMap.myStations==null){
                    bAreStationsNullOrEmpty = true;
                }
                else if(DrawMarkersOnMap.myStations.size()==0){
                    bAreStationsNullOrEmpty = true;
                }
                else {
                    bAreStationsNullOrEmpty = false;
                }

                boolean bIsBalloonNull = (mapFragment.balloon == null);

                boolean bNeedToClearMapBeforeQuit = !bAreStationsNullOrEmpty || !bIsBalloonNull;
                if(bNeedToClearMapBeforeQuit){
                    DrawMarkersOnMap.clearStations();
                    mapFragment.mMap.clear();
                    mapFragment.balloon = null;
                    final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.locate_dest_autoCompleteTextView);
                    autoCompleteTextView.setText(getString(R.string.Your_location));
                    if(mapFragment.pathFuelJumpButton != null)
                        mapFragment.pathFuelJumpButton.hide();
                    if(mapFragment.wayPointJumpButton != null)
                        mapFragment.wayPointJumpButton.hide();

                    return true;
                }
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    // by Nurlan on 12.05.2015
    // Uploads log file to server
    // Based on http://stackoverflow.com/questions/21376450/sending-audio-file-from-android-to-servlet
    private class sendFile extends AsyncTask<String, Void, String>
    {
        static final int BUFFER_SIZE = 4096;
        @Override
        protected String doInBackground(String... urls) {
            HttpURLConnection httpConn=null;
            File uploadFile = null;

            try
            {
                String file = MyLog.sFilePath + MyLog.sFileName;
                uploadFile = new File(file);
                FileInputStream inputStream = new FileInputStream(uploadFile);

                // Build the query URL
                Uri builtUri = Uri.parse(URLConstant.UPLOAD_LOG_FILE_BASE_URL).buildUpon().build();

                URL url = null;
                try {
                    url = new URL(builtUri.toString());
                } catch (MalformedURLException e) {
                    MyLog.e(LOG_TAG, "Error malformed URL from: " + builtUri.toString());
                }
                // create a HTTP connection

                httpConn = (HttpURLConnection) url.openConnection();
                httpConn.setUseCaches(false);
                httpConn.setDoOutput(true);
                httpConn.setRequestMethod("POST");
                String sFileName = uploadFile.getName();
                httpConn.setRequestProperty("fileName", sFileName);
                httpConn.connect();

                // open output stream of the HTTP connection for writing data
                OutputStream outputStream = httpConn.getOutputStream();

                // Open input stream of the file for reading data
                byte[] buffer = new byte[BUFFER_SIZE];
                int bytesRead = -1;

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
            }
            catch(SocketTimeoutException e)
            {
                MyLog.e(LOG_TAG, "error: " + e.getMessage());
            }
            catch (MalformedURLException ex)
            {
                MyLog.e(LOG_TAG, "error: " + ex.getMessage());
            }
            catch (IOException ioe)
            {
                MyLog.e(LOG_TAG, "error: " + ioe.getMessage());
            }

            try
            {
                // always check HTTP response code from server
                int responseCode = httpConn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    // delete the log file
                    try
                    {
                      if(uploadFile != null){
                        uploadFile.delete();
                      }
                    }catch(Exception ex){
                        MyLog.d(LOG_TAG, "Error deleting log file." + ex.getMessage());
                    }

                } else {
                    MyLog.d(LOG_TAG, "Server returned non-OK code: " + responseCode);
                }
            }
            catch (IOException ioex){
                MyLog.e(LOG_TAG, "error: " + ioex.getMessage());
            }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

}


