package unimelb.cis.spatialanalytics.fuelpriceshare.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import unimelb.cis.spatialanalytics.fuelpriceshare.R;

/**
 * Created by Nurlan Kenzhebekov on 24.04.2015
 * Help Fragment
 */

public class HelpFragment extends Fragment {

    private final String TAG = HelpFragment.class.getSimpleName();

    public HelpFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_help, container, false);

        return rootView;
    }

    /**
     * Receive all the returning results of children activities. Detailed information please refer to the
     * official android programming document
     *
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

    }
}


