package unimelb.cis.spatialanalytics.fuelpriceshare.maps.myLocation;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import unimelb.cis.spatialanalytics.fuelpriceshare.fragment.MapFragment;
import unimelb.cis.spatialanalytics.fuelpriceshare.others.MyLog;

/**
 * Created by Yu Sun on 26/02/2015.
 * Used exclusively (only) for ContributePriceFragment to get current user location
 * when choosing the fuel stations nearby.
 */
//@Deprecated // by Yu Sun and Han Li on 04/03/2015
// Re-used by Yu Sun on 07/04/2015

// UPDATED by Nurlan Kenzhebekov on 23.04.2015
// Now the class is used to determine user's location using Google's new more powerful Fused Location Provider API
/*
*  The Google Location Services API, part of Google Play Services, provides a more powerful, high-level framework that
*  automatically handles location providers, user movement, and location accuracy. It also handles location update scheduling
*  based on power consumption parameters you provide. In most cases, you'll get better battery performance, as well as more
*  appropriate accuracy, by using the Location Services API.
*  Source: http://developer.android.com/guide/topics/location/strategies.html
* */

// TODO: Need to test this on various Android devices!

public class MyLocation implements LocationListener, GoogleApiClient.ConnectionCallbacks,
                                   GoogleApiClient.OnConnectionFailedListener{

    private String LOG_TAG = MyLocation.class.getSimpleName();
    private final long INTERVAL = 1000 * 60;// time interval to update location in ms
    private final long FASTEST_INTERVAL = 1000 * 60;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private FusedLocationProviderApi fusedLocationProviderApi;
    private Context mainActivityContext;
    public static GoogleApiClient mGoogleApiClient;

    public MyLocation (Context context){
        this.mainActivityContext = context;
    }

    /**
    * @return user's location
    */
    public static LatLng getMyLocation(){
        //Log.v(LOG_TAG, "The current location is " + MapFragment.currentLocation.toString());
        return MapFragment.currentLocation;
    }

    /**
    * Method currently uses LocationManager to determine location of a user when the app is started
    * Later this should be changed to use fusedLocationProviderApi developed by Google as
    * it is believed more efficient
    */
    public void setUpCurrentLocation() {
        // Get LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager)mainActivityContext.getSystemService(Context.LOCATION_SERVICE);
        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(true);  // may require data transformation from ISP
        criteria.setSpeedRequired(false);
        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        Location myLocation = locationManager.getLastKnownLocation(provider);
        if(myLocation != null){
            MapFragment.currentLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
        }
    }

    /**
     * Function initializes instance of GoogleApiClient and location request
     * and connects it to google play service provider
     * @return false if Google play Service is unavailable, true otherwise
     * */
    public boolean initFusedLocationClient(){

        if (!isGooglePlayServicesAvailable()) {
            return false;
        }
        fusedLocationProviderApi = LocationServices.FusedLocationApi;
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(mainActivityContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if(!mGoogleApiClient.isConnected())
            mGoogleApiClient.connect();

        return true;
    }

    /**
     * Initializes location request object
    */
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Function checks if the GooglePlayService available
     * @return true if connected to GooglePlayService successfully, false otherwise
     */
    private boolean isGooglePlayServicesAvailable() {
        //Verifies that Google Play services is installed and enabled on this device, and that
        // the version installed on this device is no older than the one required by this client.
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mainActivityContext);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * We want continuous location updates. So initilaize constantly location update requests.
    */
    public void startLocationUpdates() {
        PendingResult<Status> pendingResult = fusedLocationProviderApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }

    /**
     * Method stops requests to determine user's location
    */
    public void stopLocationUpdates() {
        if(mGoogleApiClient!=null && mGoogleApiClient.isConnected())
            fusedLocationProviderApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    /**
     * This is invoked when Fused Location Provider API client connects to location provider
     */
    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
        //Location myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    /*
    * Invokes when user's location has been changed.
    * So we update currentLocation of the user
    */
    @Override
    public void onLocationChanged(Location location) {
        MyLog.d(LOG_TAG, "Firing onLocationChanged..............................................");
        mCurrentLocation = location;
        MapFragment.currentLocation = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        MyLog.d(LOG_TAG, "Connection failed: " + connectionResult.toString());
    }
}
