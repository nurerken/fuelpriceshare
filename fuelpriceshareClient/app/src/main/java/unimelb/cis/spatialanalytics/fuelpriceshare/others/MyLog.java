package unimelb.cis.spatialanalytics.fuelpriceshare.others;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.FileHandler;

/**
 * Created by Nurlan on 09.05.2015.
 * Write the logs to the file
 */

public class MyLog {

    private static FileHandler logger = null;
    public static String sFilePath = "/sdcard/Files/FuelPriceShare/";
    public static String sFileName = "FuelPriceShare_Log.txt";
    private static boolean isExternalStorageAvailable = false;
    private static boolean isExternalStorageWriteable = false;
    private static String state = Environment.getExternalStorageState();

    public static void d(String TAG, String message){
        Log.d(TAG, message);
        addRecordToLog(message);
    }

    public static void i(String TAG, String message){
        Log.i(TAG, message);
        addRecordToLog(message);
    }

    public static void e(String TAG, String message){
        Log.e(TAG, message);
        addRecordToLog(message);
    }

    private static void addRecordToLog(String message) {

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            isExternalStorageAvailable = isExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            isExternalStorageAvailable = true;
            isExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            isExternalStorageAvailable = isExternalStorageWriteable = false;
        }

        File dir = new File(sFilePath);
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            if(!dir.exists()) {
                dir.mkdirs();
                Log.d("Dir created ", "Dir created ");
            }

            File logFile = new File(sFilePath + sFileName);

            if (!logFile.exists())  {
                try  {
                    logFile.createNewFile();
                    Log.d("File created ", "File created ");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            try {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));

                buf.write(message + "\r\n");
                //buf.append(message);
                buf.newLine();
                buf.flush();
                buf.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}