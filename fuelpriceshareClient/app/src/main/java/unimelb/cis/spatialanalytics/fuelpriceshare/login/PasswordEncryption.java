package unimelb.cis.spatialanalytics.fuelpriceshare.login;

import android.util.Base64;

import java.security.SecureRandom;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Created by Nurlan on 02.05.2015.
 * Used to encrypt user password using
 * Password-Based Key Derivation Function 2 which
 * is a key derivation function that is part of RSA Laboratories'
 * Public-Key Cryptography Standards (PKCS) series
 */

public class PasswordEncryption {
    // The higher the number of iterations the more
    // expensive computing the hash is for us and
    // also for an attacker.
    private static final int iterations = 1000;
    // length of salt. salt is an additional string which is
    // added into password. Its purpose is differentiate two or more same user passwords
    private static final int saltLen = 32;
    private static final int desiredKeyLen = 256; // length of key

    /** */

    /**
    * @param password -- the activity the map will be presented
    * @return -- returns salted hash with the password
    * Computes a salted PBKDF2 hash of given plaintext password
    * suitable for storing in a database.
    * Empty passwords are not supported.
    */
    public static String getSaltedHash(String password) throws Exception {
        byte[] salt = SecureRandom.getInstance("SHA1PRNG").generateSeed(saltLen);//generate random numbers
        String s1 = Base64.encodeToString(salt, Base64.DEFAULT);
        String s2 = hash(password, salt);
        return s1 + "$" + s2;
    }

    /** Checks whether given plaintext password corresponds
     * to a stored salted hash of the password.
     * @param password -- user password
     * @param stored -- password stored in database
     * @return --true if hash of user password matches the password stored in database
     */
    public static boolean passwordMatches(String password, String stored) throws Exception{
        String[] saltAndPass = stored.split("\\$");
        if (saltAndPass.length != 2) {
            throw new IllegalStateException(
                    "The stored password have the form 'salt$hash'");
        }
        String hashOfInput = hash(password, Base64.decode(saltAndPass[0],Base64.DEFAULT));
        return hashOfInput.equals(saltAndPass[1]);
    }

    /**
     * Hashes user password and salt using PBKDF2
     * @param password -- user password
     * @param salt -- salt
     * @return -- hashed password
     * */
    private static String hash(String password, byte[] salt) throws Exception {
        if (password == null || password.length() == 0)
            throw new IllegalArgumentException("Empty passwords are not supported.");
        SecretKeyFactory f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey key = f.generateSecret(new PBEKeySpec(
                        password.toCharArray(), salt, iterations, desiredKeyLen)
        );
        return Base64.encodeToString(key.getEncoded(), Base64.DEFAULT);
    }
}
