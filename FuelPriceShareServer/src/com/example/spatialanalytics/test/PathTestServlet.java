package com.example.spatialanalytics.test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.example.spatialanalytics.servlet.RangeQueryServlet;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import com.spatialanalytics.config.ConstantConfig;

/**
 * Servlet implementation class PathTestServlet
 */
public class PathTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	

	private static final Logger logger = LogManager.getLogger(RangeQueryServlet.class.getSimpleName());

	// Added by Nurlan on 13 of May 2015. Use GraphHopper API to calculate rouet distance between geolocations
	private String sOSMFile; // = "C:\\Project\\graphhopper\\australia-latest.osm.pbf";// map of Australia
	private String sGraphHopperLocation; // = "C:\\Project\\graphhopper\\australia-latest.osm-gh";// folder where necessary files are stored
	private GraphHopper hopper = null;

	private static String root;
	private String rootFolder=ConstantConfig.KEY_GRAPHHOPPER_FOLDER;//the folder stores the graphhopper data

       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PathTestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    
    

	// Added by Nurlan on 13 of May 2015. Initialize graphhopper API
	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		root = config.getServletContext().getRealPath("/");
	//	root = new File(root).getParent()+"/";

		createDir(new File(root + rootFolder));
		sOSMFile = root + rootFolder + "australia-latest.osm.pbf";
		sGraphHopperLocation = root + rootFolder + "australia-latest.osm-gh";		
		logger.debug("File Folder PATH : " + root);


		hopper = new GraphHopper().forDesktop();    	
		hopper.setInMemory();
		hopper.setOSMFile(sOSMFile);
		hopper.setGraphHopperLocation(sGraphHopperLocation);
		hopper.setEncodingManager(new EncodingManager("car"));

		//now this can take minutes if it imports or a few seconds for loading
		//this is dependent on the area you import
		hopper.importOrLoad();
	}

    

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();

		out.println("root is "+root);
		out.println("sGraphHopperLocation is :"+sGraphHopperLocation);
	}

	
	/**
	 * create folder if the path doesn't exist
	 * @param file
	 */
	public void createDir(File file){
		if (!file.exists()) {
			if (file.mkdir()) {
				logger.debug("Directory is created!");
			} else {
				logger.debug("Failed to create directory!");
			}
		}
	}

}
