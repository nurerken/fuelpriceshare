package com.example.spatialanalytics.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.example.spatialanalytics.function.*;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import com.spatialanalytics.config.ConstantConfig;


/**
 * Yu Sun 30/01/2015: To provide the range query service.
 * Servlet implementation class RangeQueryServlet
 * 
 * Update history:
 * Yu Sun 01/04/2015: Check the remaining credit of the user, only if the
 * remaining credit is larger than or equal to the query credit deduction,
 * we return the query result and deduct query credit from the user's total
 * credit.
 * 
 */
//@WebServlet("/RangeQueryServlet")
public class RangeQueryServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = LogManager.getLogger(RangeQueryServlet.class.getSimpleName());

	// Added by Nurlan on 13 of May 2015. Use GraphHopper API to calculate rouet distance between geolocations
	private String sOSMFile; // = "C:\\Project\\graphhopper\\australia-latest.osm.pbf";// map of Australia
	private String sGraphHopperLocation; // = "C:\\Project\\graphhopper\\australia-latest.osm-gh";// folder where necessary files are stored
	private GraphHopper hopper = null;

	private static String root;
	private String rootFolder=ConstantConfig.KEY_GRAPHHOPPER_FOLDER;//the folder stores the graphhopper data

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RangeQueryServlet() {
		super();
	}

	// Added by Nurlan on 13 of May 2015. Initialize graphhopper API
	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
				
		//Changed by Nurlan on 17 June 2015
		//changed path to graphhopper map because of the permission rules of the real server
		//root = config.getServletContext().getRealPath("/");
		//root = new java.io.File(getServletContext().getRealPath("")).getParent() + "\\";//original one
		
		//Modified by Han and Nurlan at June 22th, 2015.
		root = ConstantConfig.KEY_GRAPHHOPPER_ROOT;		
				
		createDir(new File(root + rootFolder));
		sOSMFile = root + rootFolder + "australia-latest.osm.pbf";
		sGraphHopperLocation = root + rootFolder + "australia-latest.osm-gh";		
		logger.debug("File Folder PATH : " + root);
				
		hopper = new GraphHopper().forDesktop();    	
		hopper.setInMemory();
		hopper.setOSMFile(sOSMFile);
		hopper.setGraphHopperLocation(sGraphHopperLocation);
		hopper.setEncodingManager(new EncodingManager("car"));
		 
		//now this can take minutes if it imports or a few seconds for loading
		//this is dependent on the area you import
		hopper.importOrLoad();
	}


	/**
	 * create folder if the path doesn't exist
	 * @param file
	 */
	public void createDir(File file){
		if (!file.exists()) {
			if (file.mkdir()) {
				logger.debug("Directory is created!");
			} else {
				logger.debug("Failed to create directory!");
			}
		}
	}

	/**
	 * Yu Sun 30/01/2015 <p>
	 * The request parameter is 
	 * 		a quadruple of <latitude, longitude, range distance, user id>, 
	 * 		e.g., http://host:8080/RangeQueryServlet?lat=-37.842716&lng=144.883618&r_dist=3&user_id=222
	 * 
	 * Yu Sun 06/02/2015
	 * The returned result is a string representing a list of points (fuel stations) in the format
	 * of {"%table_name (fuel_station)":["list of points (fuel stations)"]}
	 * 
	 * Yu Sun 01/04/2015
	 * Add another query parameter of user id to check the validness of the query
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//non-optional parameter
		String range_dist = request.getParameter(com.example.spatialanalytics.function.ConstantConfig.PARAM_RANGE_DISTANCE);
		String user_id = request.getParameter(com.example.spatialanalytics.function.ConstantConfig.PARAM_CREDIT_USER_ID);
		String lat = request.getParameter(com.example.spatialanalytics.function.ConstantConfig.PARAM_LATITUDE);
		String lng = request.getParameter(com.example.spatialanalytics.function.ConstantConfig.PARAM_LONGITUDE);

		RangeQuery rq = null;
		if( (range_dist != null && !range_dist.isEmpty()) 
				&& (lat != null && !lat.isEmpty()) 
				&& (lng != null && !lng.isEmpty())
				){
			//query with the GPS coordinates
			rq = new RangeQuery(
					user_id,
					Double.valueOf(lat),			//latitude
					Double.valueOf(lng),			//longitude
					Double.valueOf(range_dist),		//range distance
					hopper                          // pass the hopper to calculate route distance 
					);
		}
		else{ 
			logger.error("Bad request from: " + request.getQueryString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.getWriter().print("Wrong parameters, please check the API doc" );
			response.getWriter().flush();
			return;
		}

		logger.trace("Getting the query result...");
		response.getWriter().print( rq.getResult() );
		response.getWriter().flush();
		logger.trace("done!");
	}

	/**
	 * Do the same as doGet.
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}