package com.spatialanalytics.servlet;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.FileCleanerCleanup;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileCleaningTracker;
import org.json.JSONObject;

import com.spatialanalytics.config.ConstantConfig;
import com.spatialanalytics.model.MyExceptionHandler;

// Created by Nurlan on 13.05.2015
// The class intended to upload log file to the server

public class UploadLogFileServlet extends HttpServlet {
   
	private static final long serialVersionUID = 1L;
	    private static final int BUFFER_SIZE = 4096; // buffer size for inputstream
    private static final Logger logger = LogManager.getLogger(UploadLogFileServlet.class.getSimpleName());
   
    private final  String TAG="UploadLogFileServlet";
	private final  MyExceptionHandler myExceptinHandler=new MyExceptionHandler(TAG);
	
	private static String root;
	private String rootFolder=ConstantConfig.KEY_LOG_FOLDER;//the folder store the uploaded log file

	private String filePath;

	public UploadLogFileServlet() {
		super();		
	}
	
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
         doPost(req, resp);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	java.io.PrintWriter out = response.getWriter( );
    	try
    	{    			    	
    		// Gets file name for HTTP header
    		String fileName = request.getHeader("fileName");

    		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    		Calendar cal = Calendar.getInstance();
    		fileName = dateFormat.format(cal.getTime()).toString() + fileName;// make unique the name of each file by adding current date time
    		File saveFile = new File(filePath + fileName);

    		// prints out all header values
    		System.out.println("===== Begin headers =====");
    		Enumeration<String> names = request.getHeaderNames();
    		while (names.hasMoreElements()) {
    			String headerName = names.nextElement();
    			//System.out.println(headerName + " = " + request.getHeader(headerName));
    		}
    		System.out.println("===== End headers =====\n");

    		// opens input stream of the request for reading data
    		InputStream inputStream = request.getInputStream();

    		// opens an output stream for writing file
    		FileOutputStream outputStream = new FileOutputStream(saveFile);

    		byte[] buffer = new byte[BUFFER_SIZE];
    		int bytesRead = -1;
    		System.out.println("Receiving data...");

    		while ((bytesRead = inputStream.read(buffer)) != -1) {
    			outputStream.write(buffer, 0, bytesRead);
    		}

    		System.out.println("Data received.");
    		outputStream.close();
    		inputStream.close();

    		// sends response to client
    		response.getWriter().print("UPLOAD DONE");
    	}
		catch(Exception ex) 
		{
			out.println(myExceptinHandler.getJsonError(ex.toString()));
			return;
		}
    }

	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		super.init(config);
		root = config.getServletContext().getRealPath("/");
		filePath = root + rootFolder;
		createDir(new File(filePath));
		logger.debug("File Folder PATH : " +root);
	}
	
	/**
	 * create folder if the path doesn't exist
	 * @param file
	 */
	public void createDir(File file){
		if (!file.exists()) {
			if (file.mkdir()) {
				logger.debug("Directory is created!");
			} else {
				logger.debug("Failed to create directory!");
			}
		}
	}

    
    
}